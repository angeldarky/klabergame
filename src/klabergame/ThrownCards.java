/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klabergame;

import java.util.ArrayList;
import java.util.List;

/**
 * Dobott lapok osztálya
 * @author KisCsaládom
 */
public class ThrownCards {

    private List<Card> thrownCards = new ArrayList();
    private int whoCouldWin; //ki nyerheti a kört
    private int callColor; //mi a hívás színe;
    // private int roundPoints = 0;
    private int whoIsComing;
    private List<Integer> whoCalled = new ArrayList();

    /* public ThrownCards() {

     }*/

    /**
     * Kártya hozzáadása
     * @param card
     * @param whoSaid
     */
    
    public void addCard(Card card, int whoSaid) {
        thrownCards.add(card);
        whoCalled.add(whoSaid);

    }

    /**
     * Kártya lekérése
     * @param index
     * @return
     */
    public Card getCard(int index) {
        return thrownCards.get(index);
    }

    /**
     * Ki dobta a lapot
     * @param card
     * @return
     */
    public int getWhoCalled(Card card) {
        int who = thrownCards.indexOf(card);
        return whoCalled.get(who);
    }

    /**
     * Megadja ki dobott elsőnek
     * @param trumpSuit
     * @return
     */
    public int getPlayerNumberWhoIsFirst(int trumpSuit) {
        int theFirst = 0;
        Card max;
        List<Card> thrownTrumps = new ArrayList<>();
        for (int i = 0; i < thrownCards.size(); i++) {
            //ha a lap tromf "színű", akkor begyűjtöm a listámba
            if (thrownCards.get(i).getSuit() == trumpSuit) {
               // System.out.println("A tromf getPlayerNumberWhoIsFirst: " + callColor);
                thrownTrumps.add(thrownCards.get(i));
            }
        }

        //ha üres a tromfos lista, vagyis nincs a dobott lapok között tromf, megkeresem a lapok közül a legnagyobbat
        if (thrownTrumps.isEmpty()) {
            max = thrownCards.get(0);
            for (int i = 1; i < 4; i++) {
                if (thrownCards.get(i).getCardPoint(thrownCards.get(i).getValue(), false) > max.getCardPoint(max.getValue(), false)) {
                    max = thrownCards.get(i);
                }
            }
            theFirst = getWhoCalled(max);

        } else {
            //ha van tromf, s csak egy, akkor lekérem, hogy ki dobta azt a lapot
            if (thrownTrumps.size() == 1) {
                theFirst = getWhoCalled(thrownTrumps.get(0));
            } else if (thrownTrumps.size() > 1) {
                //ha van tromf, de több mint egy, akkor kikeresem a legnagyobb lapot, s utána kikérem, hogy ki dobta
                max = thrownTrumps.get(0);
                for (int i = 1; i < thrownTrumps.size(); i++) {
                    if (thrownTrumps.get(i).getCardPoint(thrownTrumps.get(i).getValue(), true) > max.getCardPoint(max.getValue(), true)) {
                        max = thrownTrumps.get(i);
                    }
                }
                theFirst = getWhoCalled(max);
            }

        }

        return theFirst;
    }

    /**
     * Ki nyerheti a kört
     * @return
     */
    public int getWhoCouldWin() {
        return whoCouldWin;
    }

    /**
     * Tromf színének beállítása
     * @param suit
     */
    public void setCallColor(int suit) {
        this.callColor = suit;
    }

    /**
     * Tromf színének lekérése
     * @return
     */
    public int getCallColor() {
        return callColor;
    }

    /**
     * Beállítja, hogy ki következik
     * @param player
     */
    public void setWhoIsComing(int player) {
        this.whoIsComing = player;
    }

    /**
     * Megadja, hogy ki kovetkezik
     * @return
     */
    public int getWhoIsComing() {
        return whoIsComing;
    }

    /**
     * Dobott lapok listájának nagysága
     * @return
     */
    public int getSize() {
        return thrownCards.size();
    }

   
    /**
     *Lista üritése
     */
    
    public void resetThrownCards() {
        whoCalled.clear();
        thrownCards.clear();
    }
}
