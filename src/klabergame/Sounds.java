/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package klabergame;

import java.io.File;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author Komaromi Zsofia
 */
public class Sounds {
    public static AudioClip soundClip;
    
    /**
     * A hangok lejátszása
     * @param soundName
     */
    public static void playSound(String soundName) {
        /*soundClip = new AudioClip(soundName.toString());
        soundClip.play(1.0);*/
        MediaPlayer mediaPlayer = new MediaPlayer(new Media(new File(soundName).toURI().toString()));
        mediaPlayer.play();
    }
}
