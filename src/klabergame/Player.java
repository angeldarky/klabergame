package klabergame;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;

/**
 * Játékos osztály
 * @author KisCsaládom
 */
public class Player {
    
    private List<Card> hand;
    private final int number;
    //private boolean isOne = false;
    private int score = 0;
    private int rank = 0;
    private int trump = 0;
    
    private boolean terc = false;
    private boolean kvart = false;
    //private int cats = 0;
  //  private int rakes = 0;
    
    /**
     * Konstruktor
     * @param number
     */
    public Player(int number) {
        this.number = number;
        this.hand = new ArrayList();
    }
    
    /**
     * Visszadaj a játékos számát
     * @return
     */
    public int getPlayerNumber() {
        return number;
    }
    
    /**
     * Visszaadja a kért lapot
     * @param index
     * @return
     */
    public Card getCard(int index) {
        return hand.get(index);
    }
    
    /**
     * Megnézi, hogy lap benne van-e a kézben
     * @param value
     * @return
     */
    public int checkIsCardInHand(int value) {
        for (int i = 0; i < 8; i++) {
            if (hand.get(i).getValue() == value && !hand.get(i).getThrown()) {
                //ha a keresett kártya a kezében van, akkor elküldi, hogy melyik helyen van
                return i;
            } else {
                //ha nincs olyan kártya a kezében
                return 0;
            }
            
        }
        return 0;
        
    }
    
    /**
     * Nulláza a pontokat
     */
    public void resetScore() {
        this.score = 0;
    }

    /**
     * Lap hozzáadása
     * @param aHand
     */
    public void addToHand(List<Card> aHand) {
        this.hand = aHand;
    }
    
    /**
     * Kéz nagysága
     * @return
     */
    public int getHandSize() {
        return hand.size();
    }
    
    /**
     * Visszadja a lapokat
     * @return
     */
    public List<Card> getHand() {
        return hand;
    }
    
    /**
     * A kártya értéket adja meg
     * @param index
     * @return
     */
    public int getCardValue(int index) {
        int theValue = hand.get(index).getValue();
        return theValue;
    }
    
    /**
     * A kártya színét adja meg
     * @param index
     * @return
     */
    public int getCardSuit(int index) {
        int theSuit = hand.get(index).getSuit();
        return theSuit;
    }

    /**
     * Extra pontok vizsgálata
     * @param kvart
     * @param terc
     */
    public void setExtraScore(boolean kvart, boolean terc) {
      this.kvart = kvart;
      this.terc = terc;
  }

    /**
     * Extra kvart vizsgálat
     * @return
     */
    public boolean getExtraScoreKvart() {
      return kvart;
  }

    /**
     * extra terc pontok megadása
     * @return
     */
    public boolean getExtraScoreTerc() {
      return terc;
  }

    /**
     * Terc, kvart hamissá tétele
     */
    public void unsetExtraScore() {
       this.kvart = false;
       this.terc = false;
   }
    
    /**
     * A kártya képének visszaadása
     * @param index
     * @return
     */
    public Image getImageOfCard(int index) {
        return hand.get(index).getImage();
    }
    
    /**
     * Pontok beállítása
     * @param score
     */
    public void setScore(int score) {
        this.score = this.score + score;
              
    }
    
    /**
     * A pont megadása
     * @return
     */
    public int getScore() {
        return score;
    }
    
    /**
     * Sorszám megadása
     * @param rank
     */
    public void setRank(int rank) {
        this.rank = rank;
    }
    
    /**
     * Sorszám lekérése
     * @return
     */
    public int getRank() {
        return rank;
    }
    
    /**
     * A tromf beállítása
     * @param trump
     */
    public void setWantedTrump(int trump) {
        this.trump = trump;
    }
    
    /**
     * Tromf lekérése
     * @return
     */
    public int getWantedTrump() {
        return trump;
    }
    
    /**
     * Kártya indexének megadása
     * @param card
     * @return
     */
    public int getIndexOfCard(Card card) {
        return hand.indexOf(card);
    }
    
    /**
     * legnagyobb lap
     * @param suit
     * @return
     */
    public Card getHighCardOfThisSuit(int suit) {
        Card card = null;
        int maxInd =  0;
        for (int i = 0; i < 8; i++) {
            if(hand.get(i).getSuit() == suit && hand.get(i).getValue() > hand.get(maxInd).getValue()) {
                maxInd = i;
            }
        }
        if(maxInd == 9) { /*System.out.println("baj vaaan");*/ }
        return hand.get(maxInd);
    }

    /**
     * legkisebb lap
     * @param suit
     * @return
     */
    public Card getLowerCardOfThisSuit(int suit) {
        Card card = null;
        int maxInd =  7;
        for (int i = 0; i < 8; i++) {
            if(hand.get(i).getSuit() == suit && hand.get(i).getValue() < hand.get(maxInd).getValue()) {
                maxInd = i;
            }
        }
        if(maxInd == 9) { /*System.out.println("baj vaaan");*/ }
        return hand.get(maxInd);
    }
    
    @Override
    public String toString() {
        String out = "";
        String eol = "\n";
        for (int i = 0; i < 8; i++) {
            out = out + hand.get(i).toString() + eol;
        }
        return out;
    }
}
