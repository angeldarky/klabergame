/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klabergame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.scene.image.Image;

/**
 * Pakli osztálya
 * @author Komáromi Zsófia
 */
public class Deck {

    //a pakli
    private final List<Card> cards;
    //mi a tromf színe
    private int trumpSuit;
    //ki a kényszeres
    private int one;
    //ki kötött?
    private int trumpChooser;

    /**
     * A pakli osztály konstruktora
     */
    public Deck() {
        //feltöltöm a kártyapaklit 8+8+8+8 = 32 lap
        this.cards = new ArrayList();
        for (int i = 1; i < 9; i++) {
            cards.add(new Card(i, Card.MAKK, new Image(getClass().getResourceAsStream("/cards/m" + i + ".jpg"))));
        }
        for (int i = 1; i < 9; i++) {
            cards.add(new Card(i, Card.ZOLD, new Image(getClass().getResourceAsStream("/cards/z" + i + ".jpg"))));
        }
        for (int i = 1; i < 9; i++) {
            cards.add(new Card(i, Card.TOK, new Image(getClass().getResourceAsStream("/cards/t" + i + ".jpg"))));
        }
        for (int i = 1; i < 9; i++) {
            cards.add(new Card(i, Card.PIROS, new Image(getClass().getResourceAsStream("/cards/p" + i + ".jpg"))));
        }
        
        trumpSuit = 0;
    }

    /**
     * Kártya értékének lekérése
     * @param index
     * @return
     */
    public int getCardValue(int index) {
        return cards.get(index).getValue();
    }

    /**
     * Beállítja a következő kényszerest.
     * @param lastOne Az utolsó kényszeres száma (playerNumber)
     */
    public void setOne(int lastOne) {
        lastOne++;
         if (lastOne == 5) {
             this.one = 1;
         } else {
             this.one = lastOne;
         }
    }

    /**
     * Visszaküldi a jelenlegi kényszeres számát
     * @return Jelenlegi kényszeres száma
     */
    public int getOne() {
        return one;
    }
    
    /**
     * Ki kéri a  tromfot
     * @param trumpChooser
     */
    public void setTrumpChooser(int trumpChooser) {
        this.trumpChooser = trumpChooser;
    }

    /**
     * Visszaküldi a tromf kérőt
     * @return
     */
    public int getTrumpChooser() {
        return trumpChooser;
    }
    
    /**
     * Minden kártya dobható újra
     */
    public void changeEveryCardToThrownable() {
        for (int i = 0; i < cards.size(); i++) {
            cards.get(i).unsetThrown();
        }
    }

    /**
     *  Kártya színének lekérése
     * @param index
     * @return
     */
    public int getCardSuit(int index) {
        return cards.get(index).getSuit();
    }
    
    /**
     * tromf beállítása
     * @param trumpSuit
     */
    public void setTrumpSuit(int trumpSuit) {
        this.trumpSuit = trumpSuit;
    }

    /**
     * Tromf lekérése
     * @return
     */
    public int getTrumpSuit() {
        return trumpSuit;
    }

    /**
     * Keverés
     */
    public void shuffleCards() {
        Collections.shuffle(cards);
    }

    /**
     * Osztás
     * @param player
     * @param place
     */
    public void dealCards(Player player, int place) {
        int from = (place * 8) - 7;
        int to = place * 8;
        List<Card> give = cards.subList(from - 1, to);
        player.addToHand(give);
    }

}
