package klabergame.misc;

import java.util.Comparator;
import klabergame.Card;

/**
 *
 * @author KisCsaládom
 */
public class CardComparatorBySuit implements Comparator<Card> {

    @Override
    public int compare(Card o1, Card o2) {
        Integer x1 = ((Card) o1).getSuit();
        Integer x2 = ((Card) o2).getSuit();
        int sComp = x1.compareTo(x2);

        if (sComp != 0) {
            return sComp;
        } else {
            Integer x3 = ((Card) o1).getValue();
            Integer x4 = ((Card) o2).getValue();
            return x3.compareTo(x4);
        }
    }

}
