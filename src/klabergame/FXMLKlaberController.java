package klabergame;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.FadeTransitionBuilder;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.util.Duration;
import klabergame.misc.CardComparatorBySuit;
import klabergame.misc.Stages;

/**
 *
 * @author Komáromi Zsófia NC02ZR
 */
public class FXMLKlaberController implements Initializable {

    @FXML
    public AnchorPane menuPane;
    @FXML
    public AnchorPane settingsPane;
    @FXML
    public BorderPane tablePane;
    @FXML
    public StackPane leftTablePane;
    @FXML
    public StackPane rightTablePane;
    @FXML
    public StackPane upperTablePane;
    @FXML
    public static AnchorPane scorePane;
    @FXML
    public AnchorPane helpPane;
    @FXML
    public AnchorPane catPane;
    @FXML
    public AnchorPane rakePane;
    @FXML
    public AnchorPane getCatPane;
    @FXML
    public AnchorPane getRakePane;

    @FXML
    public HBox hbPlayer;

    @FXML
    public Button btnNewGame;

    @FXML
    public Button btnSettings;
    @FXML
    public Button btnRules;
    @FXML
    public Button btnExit;
    @FXML
    public Button btnSBack;
    @FXML
    public Button btnBack;
    @FXML
    public Button btnScore;
    @FXML
    public Button btnScoreBack;
    @FXML
    public Button btnCloseHelp;

    @FXML
    public Button btnNext;
    @FXML
    public Button btnOne;
    @FXML
    public Button btnTwo;
    @FXML
    public Button btnThree;
    @FXML
    public Button btnFour;

    @FXML
    public Button btnNext2;
    @FXML
    public Button btnOne2;
    @FXML
    public Button btnTwo2;
    @FXML
    public Button btnThree2;
    @FXML
    public Button btnFour2;

    @FXML
    public Circle circ;
    @FXML
    public Circle circ2;

    @FXML
    public ImageView cardComp1;

    @FXML
    public ImageView upCard;
    @FXML
    public ImageView leftCard;
    @FXML
    public ImageView rightCard;
    @FXML
    public RadioButton rdbDiffEasy;
    @FXML
    public RadioButton rdbDiffHard;
    @FXML
    public RadioButton rdbSoundOn;
    @FXML
    public RadioButton rdbSoundOff;
    @FXML
    public RadioButton rdbTipsOn;
    @FXML
    public RadioButton rdbTipsOff;
    @FXML
    public Label szaz;
    @FXML
    public Label otven;

    @FXML
    public Label p2;
    @FXML
    public Label p3;
    @FXML
    public Label p4;
    @FXML
    public Label lblTrump;
    @FXML
    public Label lblHelp;
    @FXML
    public Label lblLeftScore;
    @FXML
    public Label lblRightScore;

    @FXML
    public Label h1P1;
    @FXML
    public Label h2P1;
    @FXML
    public Label h3P1;
    @FXML
    public Label h4P1;
    @FXML
    public Label h5P1;
    @FXML
    public Label fiftyP1;
    @FXML
    public Label twentiesP1;
    @FXML
    public Label otherP1;

    @FXML
    public Label h1P2;
    @FXML
    public Label h2P2;
    @FXML
    public Label h3P2;
    @FXML
    public Label h4P2;
    @FXML
    public Label h5P2;
    @FXML
    public Label fiftyP2;
    @FXML
    public Label twentiesP2;
    @FXML
    public Label otherP2;

    @FXML
    public Label m1P2;
    @FXML
    public Label m2P2;
    @FXML
    public Label m3P2;
    @FXML
    public Label m4P2;
    @FXML
    public Label m5P2;

    @FXML
    public Label m1P1;
    @FXML
    public Label m2P1;
    @FXML
    public Label m3P1;
    @FXML
    public Label m4P1;
    @FXML
    public Label m5P1;

    int scoreP1 = 0;
    int scoreP2 = 0;
    int wantTrump;
    boolean newRound = false;
    Player player1 = new Player(1);
    Player player2 = new Player(2);
    Player player3 = new Player(3);
    Player player4 = new Player(4);
    public static int roundCounter;
    private int whoSaid;
    List<ImageView> cardsPlayer1;
    List<ImageView> cardsPlayer2;
    List<ImageView> cardsPlayer3;
    List<ImageView> cardsPlayer4;
    private ThrownCards thrownCards;
    public static boolean isDisabled;
    final Deck deck = new Deck();
    boolean isNewTrump;
    public static boolean isSoundOn = true;
    public static boolean isEasyDiffOn = true;
    public boolean isHelpOn = true;
    public static boolean isBackDisabled;
    public boolean isEnd = false;
    public int whoseRound;
    int roundOne;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnCloseHelp.setVisible(false);
        circ.setOpacity(0);
        circ2.setOpacity(0);
        final ToggleGroup rdbDiff = new ToggleGroup();
        final ToggleGroup rdbSound = new ToggleGroup();
        final ToggleGroup rdbTips = new ToggleGroup();
        rdbDiffHard.setToggleGroup(rdbDiff);
        rdbDiffEasy.setToggleGroup(rdbDiff);
        rdbDiffEasy.setSelected(true);
        rdbSoundOn.setToggleGroup(rdbSound);
        rdbSoundOff.setToggleGroup(rdbSound);
        rdbSoundOn.setSelected(true);
        rdbTipsOn.setToggleGroup(rdbTips);
        rdbTipsOff.setToggleGroup(rdbTips);
        rdbTipsOn.setSelected(true);
        rdbSound.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov,
                    Toggle old_toggle, Toggle new_toggle) {
                if (new_toggle.equals(rdbSoundOff)) {
                    isSoundOn = false;
                } else if (new_toggle.equals(rdbSoundOn)) {
                    isSoundOn = true;
                }

            }
        });

        rdbTips.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov,
                    Toggle old_toggle, Toggle new_toggle) {
                if (new_toggle.equals(rdbTipsOff)) {
                    isHelpOn = false;
                } else if (new_toggle.equals(rdbTipsOn)) {
                    isHelpOn = true;
                }

            }
        });

        rdbDiff.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov,
                    Toggle old_toggle, Toggle new_toggle) {
                if (new_toggle.equals(rdbDiffHard)) {
                    isEasyDiffOn = false;
                } else if (new_toggle.equals(rdbDiffEasy)) {
                    isEasyDiffOn = true;
                }

            }
        });

        btnOne.setVisible(false);
        btnTwo.setVisible(false);
        btnThree.setVisible(false);
        btnFour.setVisible(false);
        btnNext.setVisible(false);
        catPane.setOpacity(0);
        getCatPane.setOpacity(0);
        rakePane.setOpacity(0);
        getRakePane.setOpacity(0);
        thrownCards = new ThrownCards();
        cardsPlayer1 = new ArrayList();
        cardsPlayer2 = new ArrayList();
        cardsPlayer3 = new ArrayList();
        cardsPlayer4 = new ArrayList();
        leftCard.setImage(new Image(getClass().getResourceAsStream("/images/left8.png")));
        leftCard.setDisable(true);
        rightCard.setImage(new Image(getClass().getResourceAsStream("/images/right8.png")));
        rightCard.setDisable(true);
        upCard.setImage(new Image(getClass().getResourceAsStream("/images/up8.png")));
        upCard.setDisable(true);

        resetGame(1);
        rightTablePane.setAlignment(Pos.CENTER);
        hbPlayer.setAlignment(Pos.CENTER);

        //  szaz.setFont(myFont);
        // otven.setFont(myFont);
        //  cardsPlayer1.get(0).setDisable(true);
        // sortCards(player1, cardsPlayer1);
        final DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(3.0);
        dropShadow.setOffsetX(0f);
        dropShadow.setOffsetY(0f);
        dropShadow.setColor(Color.YELLOW);
        dropShadow.setWidth(40);
        dropShadow.setHeight(40);

        tablePane.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent e) {
                if (e.getTarget().getClass().equals(ImageView.class) && !isDisabled) {
                    ImageView card = (ImageView) e.getTarget();
                    //  System.out.println("kartyas!");
                    btnScore.setDisable(true);
                    btnBack.setDisable(true);
                    Animations.slideCardIn(card);
                    cardTossByPlayer(card);
                    isDisabled = true;////
                    //dobás után vizsgálom az eseteket
                    int Rank = player1.getRank();
                    if (Rank == 1) {
                        // enableAllNotThrownCards();
                        cardToss(player2, cardsPlayer2);
                        cardToss(player3, cardsPlayer3);
                        cardToss(player4, cardsPlayer4);
                        checkRoundEnd();
                    } else if (Rank == 2) {
                        //   enableThrownableCards();
                        cardToss(player2, cardsPlayer2);
                        cardToss(player3, cardsPlayer3);
                        checkRoundEnd();
                    } else if (Rank == 3) {
                        //   enableThrownableCards();
                        cardToss(player2, cardsPlayer2);
                        checkRoundEnd();

                    } else if (Rank == 4) {
                        //   enableThrownableCards();
                        checkRoundEnd();
                    }
                }
            }
        });

        tablePane.addEventHandler(MouseEvent.ANY, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getTarget().getClass().equals(ImageView.class) && e.getEventType().equals(MouseEvent.MOUSE_ENTERED_TARGET) && !isDisabled) {
                    ImageView card = (ImageView) e.getTarget();
                    card.setEffect(dropShadow);
                } else if (e.getTarget().getClass().equals(ImageView.class) && e.getEventType().equals(MouseEvent.MOUSE_EXITED_TARGET) && !isDisabled) {
                    ImageView card = (ImageView) e.getTarget();
                    card.setEffect(null);
                } else {
                }
            }

        });

        btnNewGame.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                //  System.out.println("clicked!");
                Animations.slideMenu(menuPane);

                Animations.appearHelp(helpPane, lblHelp, Stages.START, isHelpOn);
                //player1.setOne();
                startNewGame();
                //  System.out.println("Első kör vége!");
            }
        });

        btnOne.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (roundOne == 1) {
                    player1.setWantedTrump(1);
                    wantTrump = 1;
                    // System.out.println("WT P1: " + wantTrump);
                    whoSaid = player1.getRank();
                    lblTrump.setText("Mondás: " + wantTrump);
                    fadeOutText2(lblTrump);
                } else if (roundOne == 2) {
                    endOfTrumpSelection(1, player1.getPlayerNumber());
                }
            }
        });

        btnTwo.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (roundOne == 1) {
                    player1.setWantedTrump(2);
                    wantTrump = 2;
                    // System.out.println("WT P1: " + wantTrump);
                    whoSaid = player1.getRank();
                    lblTrump.setText("Mondás: " + wantTrump);
                    fadeOutText2(lblTrump);
                } else if (roundOne == 2) {
                    if (player1.getRank() == 1) {
                        //if(wantTrump < 2) {
                        endOfTrumpSelection(2, player1.getPlayerNumber());
                            //}/* else if (wantTrump >= 2) {

                        //}*/
                    } else if (player1.getRank() == 2 || player1.getRank() == 3 || player1.getRank() == 4) {
                        if (wantTrump == 1) {
                            endOfTrumpSelection(2, player1.getPlayerNumber());
                        }
                    }

                }
            }
        });

        btnThree.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (roundOne == 1) {
                    player1.setWantedTrump(3);
                    wantTrump = 3;
                    //  System.out.println("WT P1: " + wantTrump);
                    whoSaid = player1.getRank();
                    lblTrump.setText("Mondás: " + wantTrump);
                    fadeOutText2(lblTrump);
                } else if (roundOne == 2) {
                    if (player1.getRank() == 1) {
                        //if(wantTrump < 2) {
                        endOfTrumpSelection(3, player1.getPlayerNumber());
                            //}/* else if (wantTrump >= 2) {

                        //}*/
                    } else if (player1.getRank() == 2 || player1.getRank() == 3 || player1.getRank() == 4) {
                        if (wantTrump == 1 || wantTrump == 2) {
                            endOfTrumpSelection(3, player1.getPlayerNumber());
                        }
                    }
                }
            }
        });

        btnFour.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (roundOne == 1) {
                    player1.setWantedTrump(4);
                    wantTrump = 4;
                    //  System.out.println("WT P1: " + wantTrump);
                    whoSaid = player1.getRank();
                    lblTrump.setText("Mondás: " + wantTrump);
                    fadeOutText2(lblTrump);
                } else if (roundOne == 2) {
                    if (player1.getRank() == 1) {
                        //if(wantTrump < 2) {
                        endOfTrumpSelection(4, player1.getPlayerNumber());
                            //}/* else if (wantTrump >= 2) {

                        //}*/
                    } else if (player1.getRank() == 2 || player1.getRank() == 3 || player1.getRank() == 4) {
                        if (wantTrump == 1 || wantTrump == 2 || wantTrump == 3) {
                            endOfTrumpSelection(4, player1.getPlayerNumber());
                        }
                    }
                }
            }
        });

        btnNext.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (roundOne == 2) {
                    if (player1.getRank() == 1) {
                        //if(wantTrump < 2) {
                        if (whoSaid == player2.getPlayerNumber()) {
                            endOfTrumpSelection(player2.getWantedTrump(), whoSaid);
                        } else if (whoSaid == player3.getPlayerNumber()) {
                            endOfTrumpSelection(player3.getWantedTrump(), whoSaid);
                        } else if (whoSaid == player4.getPlayerNumber()) {
                            endOfTrumpSelection(player4.getWantedTrump(), whoSaid);
                        }
                            //}/* else if (wantTrump >= 2) {

                        //}*/
                    } else if (player1.getRank() == 2 || player1.getRank() == 3 || player1.getRank() == 4) {
                        if (wantTrump == 1 || wantTrump == 2 || wantTrump == 3) {
                            if (whoSaid == player2.getPlayerNumber()) {
                                endOfTrumpSelection(player2.getWantedTrump(), whoSaid);
                            } else if (whoSaid == player3.getPlayerNumber()) {
                                endOfTrumpSelection(player3.getWantedTrump(), whoSaid);
                            } else if (whoSaid == player4.getPlayerNumber()) {
                                endOfTrumpSelection(player4.getWantedTrump(), whoSaid);
                            }
                        }
                    }
                }
            }
        });

        btnScore.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                //  System.out.println("tabla!");
                Animations.slideScore(scorePane);
            }
        });
        btnScoreBack.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                //  System.out.println("tabla ki!");
                Animations.slideScore(scorePane);
                if (newRound) {
                    startNewGame();
                    newRound = false;
                }
            }
        });
        //  btnBack.setText("Kilépés");
        btnBack.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                Animations.slideMenu(menuPane);
                resetGame(3);
            }
        });

        btnExit.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                //   System.out.println("kilepes!");
                Platform.exit();
            }
        });

        btnRules.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                //  System.out.println("rules!");
                URI url = null;
                try {
                    url = new URI("http://www.mek.oszk.hu/07000/07056/07056.pdf");
                } catch (URISyntaxException ex) {
                    Logger.getLogger(FXMLKlaberController.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    Desktop.getDesktop().browse(url);
                } catch (IOException ex) {
                    Logger.getLogger(FXMLKlaberController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }
        );

        btnSettings.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e
            ) {
                //  System.out.println("settings!");
                Animations.slideSettings(settingsPane);
            }
        }
        );

        btnSBack.addEventHandler(MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e
                    ) {
                        //    System.out.println("settings ki!");
                        Animations.slideSettings(settingsPane);

                    }
                }
        );

    }

    //Van-e 3 lap9
    private int checkAreThree(Player player) {
        List<Integer> suitCounter = new ArrayList();
        for (int i = 0; i < 5; i++) {
            suitCounter.add(player.getCardSuit(i));
        }

        int freq2 = Collections.frequency(suitCounter, 2);
        int freq3 = Collections.frequency(suitCounter, 3);
        int freq4 = Collections.frequency(suitCounter, 4);

        if (freq2 >= 3) {
            return 2;
        }
        if (freq3 >= 3) {
            return 3;
        }
        if (freq4 >= 3) {
            return 4;
        }
        return 0;
    }

    //Van-e pontosan 3 lap
    private int checkAreExactlyThree(Player player) {
        List<Integer> suitCounter = new ArrayList();
        for (int i = 0; i < 8; i++) {
            suitCounter.add(player.getCardSuit(i));
        }

        int freq1 = Collections.frequency(suitCounter, 1);
        int freq2 = Collections.frequency(suitCounter, 2);
        int freq3 = Collections.frequency(suitCounter, 3);
        int freq4 = Collections.frequency(suitCounter, 4);

        if (freq1 == 3) {
            return 1;
        }
        if (freq2 == 3) {
            return 2;
        }
        if (freq3 == 3) {
            return 3;
        }
        if (freq4 == 3) {
            return 4;
        }
        return 0;
    }

    //Van-e 4 lap
    private int checkAreExactlyFour(Player player) {
        List<Integer> suitCounter = new ArrayList();
        for (int i = 0; i < 8; i++) {
            suitCounter.add(player.getCardSuit(i));
        }

        int freq1 = Collections.frequency(suitCounter, 1);
        int freq2 = Collections.frequency(suitCounter, 2);
        int freq3 = Collections.frequency(suitCounter, 3);
        int freq4 = Collections.frequency(suitCounter, 4);

        if (freq1 == 4) {
            return 1;
        }
        if (freq2 == 4) {
            return 2;
        }
        if (freq3 == 4) {
            return 3;
        }
        if (freq4 == 4) {
            return 4;
        }
        return 0;
    }

    //Tromfválasztás vége
    private void endOfTrumpSelection(int trump, int trumpChooser) {
        String suit = "";
        if (trump == 1) {
            suit = "Makk";
        }
        if (trump == 2) {
            suit = "Zöld";
        }
        if (trump == 3) {
            suit = "Tök";
        }
        if (trump == 4) {
            suit = "Piros";
        }

        lblTrump.setText("A tromf: " + suit);
        deck.setTrumpSuit(trump);
        deck.setTrumpChooser(trumpChooser);
        isDisabled = false;
        btnTwo.setDisable(true);
        btnTwo.setVisible(false);
        btnThree.setDisable(true);
        btnThree.setVisible(false);
        btnFour.setDisable(true);
        btnFour.setVisible(false);
        btnOne.setDisable(true);
        btnOne.setVisible(false);
        btnOne2.setDisable(true);
        btnOne2.setVisible(false);
        btnNext.setDisable(true);
        btnNext.setVisible(false);

        revealThreeCards();
        sortCards(player1, cardsPlayer1);
        fadeOutText(p2);
        fadeOutText(p3);
        fadeOutText(p4);
        fadeOutTextLbl(lblTrump);
        int down = throwDownCheck(player1);
        if (down > 0) {
            //  System.out.println("Ledobás");
            if (down == 3) {
                player1.setScore(200);
            } else if (down == 6) {
                player1.setScore(150);
            } else {
                player1.setScore(100);
            }

            //animáció ledobás
            launchNewRound();
            startNewGame();
        }
    }

    //Kártyalapok felfedése
    private void revealThreeCards() {
        for (int i = 5; i < 8; i++) {
            cardsPlayer1.get(i).setImage(player1.getImageOfCard(i));
            if (player1.getRank() == 1) {
                cardsPlayer1.get(i).setDisable(false);
            } else if (player1.getRank() > 1 && player1.getRank() < 5) {
                cardsPlayer1.get(i).setDisable(true);
            }
            hbPlayer.getChildren().set(i, cardsPlayer1.get(i));
        }
    }

    private void cardTossByPlayer(ImageView card) {
        int id = cardsPlayer1.indexOf(card);

        if (thrownCards.getSize() == 0) {
            thrownCards.setCallColor(player1.getCardSuit(id));
            //  System.out.println("Dobott suit P1: " + player1.getCardSuit(id));
        }
        //   System.out.println("A dobott kartya indexe a cardsPlayer1-ben:" + id);
        thrownCards.addCard(player1.getCard(id), player1.getPlayerNumber());
        isDisabled = true;/////
        player1.getCard(id).setThrown();

        /*if (thrownCards.getSize() != 4) {
         thrownCards.setWhoIsComing(2);
         } else {
         //calc points, who had the high card
         //int highCarder;
         //thrownCards.setWhoIsComing(highCarder);
         }*/
    }

    //Számítógépes játékos dobása
    private void cardToss(Player player, List<ImageView> cardsPlayer) {
        //if (player.getPlayerNumber() == thrownCards.getWhoIsComing()) {

        Card tossCard;
        if (thrownCards.getSize() == 0) {
            //   System.out.println("Elsőnek dob... P" + player.getPlayerNumber());

            if (isEasyDiffOn) {
                whoSaid = player.getPlayerNumber();
                tossCard = player.getCard(0);
                int x = 8;
                for (int i = 0; i < 8; i++) {
                    if (!player.getCard(i).getThrown()) {
                        tossCard = player.getCard(i);

                        x = i;
                    }
                }

                if (x == 8) {
                    //     System.out.println("baj van, nem talált");
                } else {
                    thrownCards.addCard(tossCard, whoSaid);
                    player.getCard(x).setThrown();
                    thrownCards.setCallColor(player.getCardSuit(x));
                    //System.out.println("Dobtam, voltam: P" + player.getPlayerNumber());
                }

                Animations.slideCardIn2(cardsPlayer.get(whichImageViewContainsTheCard(thrownCards.getCard(thrownCards.getSize() - 1), player, cardsPlayer)), player.getPlayerNumber());
            } else if (!isEasyDiffOn) {
                //nehéz szint
                //ha elsőnek dob a cpu
                //ha van nála Ász, ami nem tromf, akkor azt dobja
                int isAce = player.checkIsCardInHand(1);
                if (isAce != 0 && player.getCardSuit(isAce) != deck.getTrumpSuit() && !player.getCard(isAce).getThrown()) {
                    whoSaid = player.getPlayerNumber();
                    thrownCards.addCard(player.getCard(isAce), whoSaid);
                    thrownCards.setCallColor(player.getCardSuit(isAce));
                    player.getCard(isAce).setThrown();
                    //animáció
                    Animations.slideCardIn2(cardsPlayer.get(isAce), player.getPlayerNumber());

                } else //ha van nem tromfja, akkor azt dobja
                {
                    whoSaid = player.getPlayerNumber();
                    tossCard = player.getCard(0);
                    int x = 8;
                    for (int i = 0; i < 8; i++) {
                        if (!player.getCard(i).getThrown()) {
                            tossCard = player.getCard(i);

                            x = i;
                        }
                    }
//System.out.println("talált: " + tossCard.toString() );

                    if (x == 8) {
                        //   System.out.println("baj van, nem talált");
                    } else {
                        thrownCards.addCard(tossCard, whoSaid);
                        player.getCard(x).setThrown();
                        thrownCards.setCallColor(player.getCardSuit(x));
                        //  System.out.println("Dobtam, voltam: P" + player.getPlayerNumber());
                    }

                    Animations.slideCardIn2(cardsPlayer.get(whichImageViewContainsTheCard(thrownCards.getCard(thrownCards.getSize() - 1), player, cardsPlayer)), player.getPlayerNumber());
                }
            }
            //ha csak tromf van nála, akkor azt
        } else if (thrownCards.getSize() == 1 || thrownCards.getSize() == 3) {
            whoSaid = player.getPlayerNumber();
            // System.out.println("whosaid2 = " + whoSaid);
            //ha már dobott valaki előtte, s a párja még nem, akkor random dob egy ugyanolyan színűt
            int callSuit = thrownCards.getCallColor();
            //System.out.println("CMP látja, hogy a dobott kartya suitja: " + callSuit);
            //keres a kezében lévő kártyák közül egy ugyanolyan színűt
            tossCard = getSameSuitCard(player, callSuit);
            if (tossCard == null) {
                //ha nincs ugyanolyan színű, akkor tromfot
                tossCard = getSameSuitCard(player, deck.getTrumpSuit());
                if (tossCard == null) {
                    //ha nincs tromf, akkor random bármit
                    //Random rand = new Random();
                    int x = 8;
                    for (int i = 0; i < 8; i++) {
                        if (!player.getCard(i).getThrown()) {
                            tossCard = player.getCard(i);

                            x = i;
                        }
                    }

                    if (x == 8) {
                        //   System.out.println("baj van, nem talált");
                    } else {
                        thrownCards.addCard(tossCard, whoSaid);
                        player.getCard(x).setThrown();
                        //     System.out.println("Dobtam, voltam: P" + player.getPlayerNumber());
                    }

                } else {
                    //tromfot dobok
                    thrownCards.addCard(tossCard, whoSaid);
                    player.getCard(player.getIndexOfCard(tossCard)).setThrown();
                    //System.out.println("Dobtam, voltam: " + player.getPlayerNumber());
                }
            } else {
                //ezt dobom
                thrownCards.addCard(tossCard, whoSaid);
                player.getCard(player.getIndexOfCard(tossCard)).setThrown();
                //System.out.println("Dobtam, voltam: " + player.getPlayerNumber());
            }

            Animations.slideCardIn2(cardsPlayer.get(whichImageViewContainsTheCard(thrownCards.getCard(thrownCards.getSize() - 1), player, cardsPlayer)), player.getPlayerNumber());
            //player.removeCardFromHand(tossCard);

        } else if (thrownCards.getSize() == 2) {
            if (!isEasyDiffOn) { //3-nak dob, már dobott a párja is
             /*
                 Abban a szituációban, mikor a számítógép párja elsőnek dob,
                 ő dob harmadiknak, vizsgálja, hogy a párja viszi-e a kört,
                 övé-e az addigi legnagyobb lap.
                 Ha igen, akkor lehetőségeihez mérten a legnagyobb lapot dobja el.
                 Ha nem, akkor pedig a lagkisebbre törekszik.*/

                whoSaid = player.getPlayerNumber();
              //  System.out.println("whosaid2 = " + whoSaid);
                //ha már dobott valaki előtte, s a párja még nem, akkor random dob egy ugyanolyan színűt
                int callSuit = thrownCards.getCallColor();
              //  System.out.println("CMP látja, hogy a dobott kartya suitja: " + callSuit);
                //keres a kezében lévő kártyák közül egy ugyanolyan színűt
                tossCard = getSameSuitCard(player, callSuit);
                if (tossCard == null) {
                    //ha nincs ugyanolyan színű, akkor tromfot
                    tossCard = getSameSuitCard(player, deck.getTrumpSuit());
                    if (tossCard == null) {
                        //ha nincs tromf, akkor random bármit
                        int x = 8;
                        for (int i = 0; i < 8; i++) {
                            if (!player.getCard(i).getThrown()) {
                                tossCard = player.getCard(i);

                                x = i;
                            }
                        }

                        if (x == 8) {
                         //   System.out.println("baj van, nem talált");
                        } else {
                            thrownCards.addCard(tossCard, whoSaid);
                            player.getCard(x).setThrown();
                        }
                    } else {
                        //tromfot dobok
                        thrownCards.addCard(tossCard, whoSaid);
                        player.getCard(player.getIndexOfCard(tossCard)).setThrown();
                    }
                   // System.out.println("Dobtam, voltam: " + player.getPlayerNumber());
                } else {
             //ha van kért színem, (vagyis a párom színe), akkor megnézem,
                    //az első és második színe egyezik-e, ha igen megnézem,
                    //hogy az első lapja nagyobb-e, mint a másodiké, s ha igen
                    //akkor nézek nagy lapot, ha nem, akkor kicsit,
                    //ha a színek különböznek

                    //két szín egyezik
                    if (thrownCards.getCard(0).getSuit() == thrownCards.getCard(1).getSuit()) {
                        //a pár lapértéke nagyobb
                        if (thrownCards.getCard(0).getValue() > thrownCards.getCard(1).getValue()) {
                            tossCard = player.getHighCardOfThisSuit(thrownCards.getCallColor());
                            //pár lapértéke kisebb
                        } else {
                            tossCard = player.getLowerCardOfThisSuit(thrownCards.getCallColor());
                        }
                        // a két szín nem egyezik meg és a második játékos tromfot tett
                    } else {
                        if (thrownCards.getCard(1).getSuit() == deck.getTrumpSuit()) {
                            //kicsi kérés színű lapot teszek
                            tossCard = player.getLowerCardOfThisSuit(thrownCards.getCallColor());
                            //ha nem tett tromfot, akkor nagy lapot teszek, mert eddig miénk a kör
                        } else {
                            tossCard = player.getHighCardOfThisSuit(thrownCards.getCallColor());
                        }
                    }
                    thrownCards.addCard(tossCard, whoSaid);
                    player.getCard(player.getIndexOfCard(tossCard)).setThrown();
                }

                Animations.slideCardIn2(cardsPlayer.get(whichImageViewContainsTheCard(thrownCards.getCard(thrownCards.getSize() - 1), player, cardsPlayer)), player.getPlayerNumber());

            } else if (isEasyDiffOn) {
                whoSaid = player.getPlayerNumber();
            // System.out.println("whosaid2 = " + whoSaid);
                //ha már dobott valaki előtte, s a párja még nem, akkor random dob egy ugyanolyan színűt
                int callSuit = thrownCards.getCallColor();
            //System.out.println("CMP látja, hogy a dobott kartya suitja: " + callSuit);
                //keres a kezében lévő kártyák közül egy ugyanolyan színűt
                tossCard = getSameSuitCard(player, callSuit);
                if (tossCard == null) {
                    //ha nincs ugyanolyan színű, akkor tromfot
                    tossCard = getSameSuitCard(player, deck.getTrumpSuit());
                    if (tossCard == null) {
                    //ha nincs tromf, akkor random bármit
                        //Random rand = new Random();
                        int x = 8;
                        for (int i = 0; i < 8; i++) {
                            if (!player.getCard(i).getThrown()) {
                                tossCard = player.getCard(i);

                                x = i;
                            }
                        }

                        if (x == 8) {
                            //   System.out.println("baj van, nem talált");
                        } else {
                            thrownCards.addCard(tossCard, whoSaid);
                            player.getCard(x).setThrown();
                            //     System.out.println("Dobtam, voltam: P" + player.getPlayerNumber());
                        }

                    } else {
                        //tromfot dobok
                        thrownCards.addCard(tossCard, whoSaid);
                        player.getCard(player.getIndexOfCard(tossCard)).setThrown();
                        //System.out.println("Dobtam, voltam: " + player.getPlayerNumber());
                    }
                } else {
                    //ezt dobom
                    thrownCards.addCard(tossCard, whoSaid);
                    player.getCard(player.getIndexOfCard(tossCard)).setThrown();
                    //System.out.println("Dobtam, voltam: " + player.getPlayerNumber());
                }

                Animations.slideCardIn2(cardsPlayer.get(whichImageViewContainsTheCard(thrownCards.getCard(thrownCards.getSize() - 1), player, cardsPlayer)), player.getPlayerNumber());
                //player.removeCardFromHand(tossCard);
            }
        }

        if (player1.getRank() == 1) {
            enableAllNotThrownCards();
        } else if (player1.getRank() >= 2 && player1.getRank() <= 4) {
            enableThrownableCards();
        }
    }

    //Melyik ImageView-hez tartozik a lap
    private int whichImageViewContainsTheCard(Card card, Player player, List<ImageView> cardsPlayer) {
        int index = 9; //ha nincs találat, akkor 9, de olyan nem lesz

        for (int i = 0; i < cardsPlayer.size(); i++) {
            if (card.getImage().equals(cardsPlayer.get(i).getImage())) {
                index = i;
            }

        }
        return index;
    }

    //lapok rendezése
    private void sortCards(Player player, List<ImageView> cardsPlayer) {
        List<Card> unsortedHand = player.getHand();
        Collections.sort(unsortedHand, new CardComparatorBySuit());

        /*  System.out.println("Rendezés után: ");
         System.out.println(unsortedHand);
         String out = "";
         String eol = "\n";
         for (int i = 0; i < 8; i++) {
         out = out + unsortedHand.get(i).toString() + eol;
         }*/
        for (int i = 0; i < cardsPlayer.size(); i++) {
            cardsPlayer.get(i).setImage(unsortedHand.get(i).getImage());
        }

    }

    //Pontok táblára írása
    private void calculateScore() {
        // System.out.println("ScoreP1: " + scoreP1);
        // System.out.println("ScoreP2: " + scoreP2);
        // System.out.println("TrumpChooser (ki kötött?): P" + deck.getTrumpChooser());
        if (deck.getTrumpChooser() == 1 || deck.getTrumpChooser() == 3) {
            if (scoreP1 > scoreP2) {
                player1.setScore(scoreP1);
                player2.setScore(scoreP2);
            } else if (scoreP1 <= scoreP2) {
                player1.setScore(0);
                player2.setScore(scoreP2);
            }
        } else if (deck.getTrumpChooser() == 2 || deck.getTrumpChooser() == 4) {
            if (scoreP2 > scoreP1) {
                player1.setScore(scoreP1);
                player2.setScore(scoreP2);
            } else if (scoreP2 <= scoreP1) {
                player2.setScore(0);
                player1.setScore(scoreP1);
            }
        }

        int score = player1.getScore();
        int score2 = player2.getScore();
        // System.out.println("s1: " + score);
        // System.out.println("s2: " + score2);
        int hundreds = 0;
        int eighty = 0;
        int sixty = 0;
        int fifty = 0;
        int fourty = 0;
        int twenty = 0;

        //Játékos + számítógép
        if (score < 501) {
            hundreds = score / 100;
            //ha van százas
            if (hundreds > 0) {
                score = score - hundreds * 100;
            }
            eighty = score / 80;
            //ha van nyolvanas
            if (eighty > 0) {
                score = score - 80;
            }
            sixty = score / 60;
            //ha van hatvanas
            if (sixty > 0) {
                score = score - 60;
            }

            fifty = score / 50;
            //ha van ötvenes
            if (fifty > 0) {
                score = score - 50;
            }
            fourty = score / 40;
            //ha van negyvenes
            if (fourty > 0) {
                score = score - 40;
            }
            twenty = score / 20;
            //ha van nyolvanas
            if (twenty > 0) {
                score = score - 20;
            }

            if (hundreds == 5) {
                h1P1.setText("O");
                h2P1.setText("O");
                h3P1.setText("O");
                h4P1.setText("O");
                h5P1.setText("O");
            }
            if (hundreds == 4) {
                h1P1.setText("O");
                h2P1.setText("O");
                h3P1.setText("O");
                h4P1.setText("O");
                h5P1.setText("");
            }
            if (hundreds == 3) {
                h1P1.setText("O");
                h2P1.setText("O");
                h3P1.setText("O");
                h4P1.setText("");
                h5P1.setText("");
            }
            if (hundreds == 2) {
                h1P1.setText("O");
                h2P1.setText("O");
                h3P1.setText("");
                h4P1.setText("");
                h5P1.setText("");
            }
            if (hundreds == 1) {
                h1P1.setText("O");
                h2P1.setText("");
                h3P1.setText("");
                h4P1.setText("");
                h5P1.setText("");
            }

            if (eighty == 1) {
                twentiesP1.setText("N");
            } else if (sixty == 1) {
                twentiesP1.setText("H");
            } else if (fourty == 1) {
                twentiesP1.setText("G");
            } else if (twenty == 1) {
                twentiesP1.setText("Z");
            } else {
                twentiesP1.setText("");
            }

            if (fifty == 1) {
                fiftyP1.setText("V");
            } else {
                fiftyP1.setText("");
            }

            if (score > 0) {
                otherP1.setText(Integer.toString(score));
            } else {
                otherP1.setText("");
            }

            lblLeftScore.setText("Pontszám: " + Integer.toString(player1.getScore()));

        }

        //Számítógép + számítógép
        if (score2 < 501) {
            hundreds = score2 / 100;
            //ha van százas
            if (hundreds > 0) {
                score2 = score2 - hundreds * 100;
            }
            eighty = score2 / 80;
            //ha van nyolvanas
            if (eighty > 0) {
                score2 = score2 - 80;
            }
            sixty = score2 / 60;
            //ha van hatvanas
            if (sixty > 0) {
                score2 = score2 - 60;
            }
            fifty = score2 / 50;
            //ha van ötvenes
            if (fifty > 0) {
                score2 = score2 - 50;
            }
            fourty = score2 / 40;
            //ha van negyvenes
            if (fourty > 0) {
                score2 = score2 - 40;
            }
            twenty = score2 / 20;
            //ha van nyolvanas
            if (twenty > 0) {
                score2 = score2 - 20;
            }

            if (hundreds == 5) {
                h1P2.setText("O");
                h2P2.setText("O");
                h3P2.setText("O");
                h4P2.setText("O");
                h5P2.setText("O");
            }
            if (hundreds == 4) {
                h1P2.setText("O");
                h2P2.setText("O");
                h3P2.setText("O");
                h4P2.setText("O");
                h5P2.setText("");
            }
            if (hundreds == 3) {
                h1P2.setText("O");
                h2P2.setText("O");
                h3P2.setText("O");
                h4P2.setText("");
                h5P2.setText("");
            }
            if (hundreds == 2) {
                h1P2.setText("O");
                h2P2.setText("O");
                h3P2.setText("");
                h4P2.setText("");
                h5P2.setText("");
            }
            if (hundreds == 1) {
                h1P2.setText("O");
                h2P2.setText("");
                h3P2.setText("");
                h4P2.setText("");
                h5P2.setText("");
            }

            if (eighty == 1) {
                twentiesP2.setText("N");
            } else if (sixty == 1) {
                twentiesP2.setText("H");
            } else if (fourty == 1) {
                twentiesP2.setText("G");
            } else if (twenty == 1) {
                twentiesP2.setText("Z");
            } else {
                twentiesP2.setText("");
            }

            if (fifty == 1) {
                fiftyP2.setText("V");
            } else {
                fiftyP2.setText("");
            }

            if (score2 > 0) {
                otherP2.setText(Integer.toString(score2));
            } else {
                otherP2.setText("");
            }

            lblRightScore.setText("Pontszám: " + Integer.toString(player2.getScore()));

        }

        if (player1.getScore() >= 501) {
            //ha elérték az 501 pontot, tábla törlése, macskák, gereblyék számolása  
            if (player2.getScore() < 251) {
                fadeIn(catPane);
            } else if (player2.getScore() < 501) {
                fadeIn(rakePane);
            } else if (player2.getScore() >= 501) {
                if (deck.getTrumpChooser() == 1 || deck.getTrumpChooser() == 3) {
                    fadeIn(rakePane);
                } else if (deck.getTrumpChooser() == 2 || deck.getTrumpChooser() == 4) {
                    fadeIn(getRakePane);
                }
            }
            player1.resetScore();
            player2.resetScore();
            //clearScoreTable();
        } else if (player2.getScore() >= 501) {
            if (player1.getScore() < 251) {
                fadeIn(getCatPane);
            } else if (player1.getScore() < 501) {
                fadeIn(getRakePane);
            }
            player1.resetScore();
            player2.resetScore();
        }
    }

    //Pontjelző nullázása
    private void initScoreLabels() {

        Font myFont = Font.loadFont(FXMLKlaberController.class
                .getResource("/fonts/Eraser.ttf").toExternalForm(), 70);

        h1P1.setText(
                "");
        h1P1.setFont(myFont);

        h2P1.setText(
                "");
        h2P1.setFont(myFont);

        h3P1.setText(
                "");
        h3P1.setFont(myFont);

        h4P1.setText(
                "");
        h4P1.setFont(myFont);

        h5P1.setText(
                "");
        h5P1.setFont(myFont);

        twentiesP1.setText(
                "");
        twentiesP1.setFont(myFont);

        fiftyP1.setText(
                "");
        fiftyP1.setFont(myFont);

        otherP1.setText(
                "");
        otherP1.setFont(myFont);

        m1P1.setText(
                "");
        m1P1.setFont(myFont);

        m2P1.setText(
                "");
        m2P1.setFont(myFont);

        m3P1.setText(
                "");
        m3P1.setFont(myFont);

        m4P1.setText(
                "");
        m4P1.setFont(myFont);

        m5P1.setText(
                "");
        m5P1.setFont(myFont);

        h1P2.setText(
                "");
        h1P2.setFont(myFont);

        h2P2.setText(
                "");
        h2P2.setFont(myFont);

        h3P2.setText(
                "");
        h3P2.setFont(myFont);

        h4P2.setText(
                "");
        h4P2.setFont(myFont);

        h5P2.setText(
                "");
        h5P2.setFont(myFont);

        twentiesP2.setText(
                "");
        twentiesP2.setFont(myFont);

        fiftyP2.setText(
                "");
        fiftyP2.setFont(myFont);

        otherP2.setText(
                "");
        otherP2.setFont(myFont);

        m1P2.setText(
                "");
        m1P2.setFont(myFont);

        m2P2.setText(
                "");
        m2P2.setFont(myFont);

        m3P2.setText(
                "");
        m3P2.setFont(myFont);

        m4P2.setText(
                "");
        m4P2.setFont(myFont);

        m5P2.setText(
                "");
        m5P2.setFont(myFont);
        lblLeftScore.setText("");
        lblRightScore.setText("");
    }

    //Ugyanolyan színű lap lekérése
    private Card getSameSuitCard(Player player, int suit) {
        for (int i = 0; i < player.getHandSize(); i++) {
            if (player.getCardSuit(i) == suit && !player.getCard(i).getThrown()) {
                //ha talál ugyanolyan színű kártyát visszaküldi
                //   System.out.println("idáig ért");
                return player.getCard(i);
            }
        }
        //  System.out.println("idáig ért2");
        //ha nincs találat null-t küld vissza
        return null;
    }

    public void resetGame(int from) {

        wantTrump = 0;
        deck.shuffleCards();
        deck.changeEveryCardToThrownable();
        deck.dealCards(player1, 1);
        deck.dealCards(player2, 2);
        deck.dealCards(player3, 3);
        deck.dealCards(player4, 4);
        deck.setTrumpSuit(0);
        //  System.out.println("Player1: " + player1);
        ///  System.out.println("Player2: " + player2);
        //   System.out.println("Player3: " + player3);
        //   System.out.println("Player4: " + player4);

//        cardComp1.setImage(player2.getImageOfCard(0));
        //      cardComp1.setOpacity(0);
        helpPane.setVisible(false);
        //   isNewTrump = true;
        thrownCards.resetThrownCards();
        //a játék indulásakor állítom erre
        if (from == 1) {
//            enableAllNotThrownCards();
            initScoreLabels();
            deck.setOne(0);
            player1.setRank(1);
            player2.setRank(2);
            player3.setRank(3);
            player4.setRank(4);
            //Settings cards for the Player
            for (int i = 0; i < 5; i++) {
                cardsPlayer1.add(new ImageView());
                cardsPlayer1.get(i).setImage(player1.getImageOfCard(i));
                cardsPlayer1.get(i).setOpacity(1);
                hbPlayer.getChildren().add(cardsPlayer1.get(i));
            }
            cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(5).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            cardsPlayer1.get(5).setOpacity(1);
            hbPlayer.getChildren().add(cardsPlayer1.get(5));
            cardsPlayer1.add(new ImageView());

            cardsPlayer1.get(6).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            cardsPlayer1.get(6).setOpacity(1);
            hbPlayer.getChildren().add(cardsPlayer1.get(6));
            cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(7).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            cardsPlayer1.get(7).setOpacity(1);
            hbPlayer.getChildren().add(cardsPlayer1.get(7));

            for (int i = 0; i < 8; i++) {
                cardsPlayer2.add(new ImageView());
                cardsPlayer2.get(i).setImage(player2.getImageOfCard(i));
                cardsPlayer2.get(i).setOpacity(0);
                rightTablePane.getChildren().add(cardsPlayer2.get(i));
            }

            for (int i = 0; i < 8; i++) {
                cardsPlayer3.add(new ImageView());
                cardsPlayer3.get(i).setImage(player3.getImageOfCard(i));
                cardsPlayer3.get(i).setOpacity(0);
                upperTablePane.getChildren().add(cardsPlayer3.get(i));
            }

            for (int i = 0; i < 8; i++) {
                cardsPlayer4.add(new ImageView());
                cardsPlayer4.get(i).setImage(player4.getImageOfCard(i));
                cardsPlayer4.get(i).setOpacity(0);
                leftTablePane.getChildren().add(cardsPlayer4.get(i));
            }

            //Új kör kezdetén hívom meg, ha eldobták mind a 8 lapot
        } else if (from == 2) {

            for (int i = 0; i < 5; i++) {
                // cardsPlayer1.add(new ImageView());
                cardsPlayer1.get(i).setTranslateX(0);
                cardsPlayer1.get(i).setTranslateY(0);
                cardsPlayer1.get(i).setOpacity(1);
                cardsPlayer1.get(i).setVisible(true);
                cardsPlayer1.get(i).setEffect(null);
                cardsPlayer1.get(i).setImage(player1.getImageOfCard(i));
                hbPlayer.getChildren().set(i, cardsPlayer1.get(i));
            }
            // cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(5).setTranslateX(0);
            cardsPlayer1.get(5).setTranslateY(0);
            cardsPlayer1.get(5).setOpacity(1);
            cardsPlayer1.get(5).setVisible(true);
            cardsPlayer1.get(5).setEffect(null);
            cardsPlayer1.get(5).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            hbPlayer.getChildren().set(5, cardsPlayer1.get(5));
            // cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(6).setTranslateX(0);
            cardsPlayer1.get(6).setTranslateY(0);
            cardsPlayer1.get(6).setOpacity(1);
            cardsPlayer1.get(6).setVisible(true);
            cardsPlayer1.get(6).setEffect(null);
            cardsPlayer1.get(6).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            hbPlayer.getChildren().set(6, cardsPlayer1.get(6));
            //  cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(7).setTranslateX(0);
            cardsPlayer1.get(7).setTranslateY(0);
            cardsPlayer1.get(7).setOpacity(1);
            cardsPlayer1.get(7).setVisible(true);
            cardsPlayer1.get(7).setEffect(null);
            cardsPlayer1.get(7).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            hbPlayer.getChildren().set(7, cardsPlayer1.get(7));

            for (int i = 0; i < 8; i++) {
                //  cardsPlayer2.add(new ImageView());
                cardsPlayer2.get(i).setTranslateX(0);
                cardsPlayer2.get(i).setTranslateY(0);
                cardsPlayer2.get(i).setOpacity(0);
                cardsPlayer2.get(i).setEffect(null);
                cardsPlayer2.get(i).setImage(player2.getImageOfCard(i));
//            rightTablePane.getChildren().set(i,cardsPlayer2.get(i));
            }

            for (int i = 0; i < 8; i++) {
                //  cardsPlayer3.add(new ImageView());
                cardsPlayer3.get(i).setTranslateX(0);
                cardsPlayer3.get(i).setTranslateY(0);
                cardsPlayer3.get(i).setOpacity(0);
                cardsPlayer3.get(i).setEffect(null);
                cardsPlayer3.get(i).setImage(player3.getImageOfCard(i));
                //   upperTablePane.getChildren().set(i,cardsPlayer3.get(i));
            }

            for (int i = 0; i < 8; i++) {
                //  cardsPlayer4.add(new ImageView());
                cardsPlayer4.get(i).setTranslateX(0);
                cardsPlayer4.get(i).setTranslateY(0);
                cardsPlayer4.get(i).setOpacity(0);
                cardsPlayer4.get(i).setEffect(null);
                cardsPlayer4.get(i).setImage(player4.getImageOfCard(i));
                //     leftTablePane.getChildren().set(i,cardsPlayer4.get(i));
            }
            // deck.changeEveryCardToThrownable();
            // enableAllNotThrownCards();
            //btnBack lenyomásakor, 
            //amikor visszamegyek a főmenübe,
            //illetve ha vége játéknak, valaki >=501 pontot szerzett
        } else if (from == 3) {
            deck.changeEveryCardToThrownable();
            initScoreLabels();

            player1.resetScore();
            player2.resetScore();
            deck.setOne(0);
            player1.setRank(1);
            player2.setRank(2);
            player3.setRank(3);
            player4.setRank(4);
            deck.setTrumpChooser(0);
            for (int i = 0; i < 5; i++) {
                // cardsPlayer1.add(new ImageView());
                cardsPlayer1.get(i).setTranslateX(0);
                cardsPlayer1.get(i).setTranslateY(0);
                cardsPlayer1.get(i).setOpacity(1);
                cardsPlayer1.get(i).setDisable(false);
                cardsPlayer1.get(i).setEffect(null);
                cardsPlayer1.get(i).setImage(player1.getImageOfCard(i));
                hbPlayer.getChildren().set(i, cardsPlayer1.get(i));
            }
            // cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(5).setTranslateX(0);
            cardsPlayer1.get(5).setTranslateY(0);
            cardsPlayer1.get(5).setOpacity(1);
            cardsPlayer1.get(5).setEffect(null);
            cardsPlayer1.get(5).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            hbPlayer.getChildren().set(5, cardsPlayer1.get(5));
            // cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(6).setTranslateX(0);
            cardsPlayer1.get(6).setTranslateY(0);
            cardsPlayer1.get(6).setOpacity(1);
            cardsPlayer1.get(6).setEffect(null);
            cardsPlayer1.get(6).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            hbPlayer.getChildren().set(6, cardsPlayer1.get(6));
            //  cardsPlayer1.add(new ImageView());
            cardsPlayer1.get(7).setTranslateX(0);
            cardsPlayer1.get(7).setTranslateY(0);
            cardsPlayer1.get(7).setOpacity(1);
            cardsPlayer1.get(7).setEffect(null);
            cardsPlayer1.get(7).setImage(new Image(getClass().getResourceAsStream("/cards/hat.jpg")));
            hbPlayer.getChildren().set(7, cardsPlayer1.get(7));

            for (int i = 0; i < 8; i++) {
                //  cardsPlayer2.add(new ImageView());
                cardsPlayer2.get(i).setTranslateX(0);
                cardsPlayer2.get(i).setTranslateY(0);
                cardsPlayer2.get(i).setOpacity(0);
                cardsPlayer2.get(i).setEffect(null);
                cardsPlayer2.get(i).setImage(player2.getImageOfCard(i));
//            rightTablePane.getChildren().set(i,cardsPlayer2.get(i));
            }

            for (int i = 0; i < 8; i++) {
                //  cardsPlayer3.add(new ImageView());
                cardsPlayer3.get(i).setTranslateX(0);
                cardsPlayer3.get(i).setTranslateY(0);
                cardsPlayer3.get(i).setOpacity(0);
                cardsPlayer3.get(i).setEffect(null);
                cardsPlayer3.get(i).setImage(player3.getImageOfCard(i));
                //   upperTablePane.getChildren().set(i,cardsPlayer3.get(i));
            }

            for (int i = 0; i < 8; i++) {
                //  cardsPlayer4.add(new ImageView());
                cardsPlayer4.get(i).setTranslateX(0);
                cardsPlayer4.get(i).setTranslateY(0);
                cardsPlayer4.get(i).setOpacity(0);
                cardsPlayer4.get(i).setEffect(null);
                cardsPlayer4.get(i).setImage(player4.getImageOfCard(i));
                //     leftTablePane.getChildren().set(i,cardsPlayer4.get(i));
            }

            // enableAllNotThrownCards();
        }
    }

    //Új játék 
    private void startNewGame() {
        isDisabled = true;

        player1.setWantedTrump(0);
        player2.setWantedTrump(0);
        player3.setWantedTrump(0);
        player4.setWantedTrump(0);

        p2.setText("");
        p2.setOpacity(1);
        p3.setText("");
        p3.setOpacity(1);
        p4.setText("");
        p4.setOpacity(1);
        lblTrump.setText("");
        lblTrump.setOpacity(1);

        trumpSelection();
    }

    //Kör végének figyelése
    private void checkRoundEnd() {
        if (roundCounter != 0) {
            if (thrownCards.getSize() == 4) {

                //mindenki dobott, pontszámlálás...
                calculatePointsOnRoundEnd();
                //       System.out.println("mindenki dobott, pontszámlálás, kitörlöm a lapokat a játkosok kezéből\n\n");
                //eltűntetem a 4 lapot
                Card card2;
                int xx;
                int who;
                for (int i = 0; i < 4; i++) {
                    //     System.out.println("for ciklus: " + i + ".");
                    //    System.out.println(thrownCards.toString());
                    card2 = thrownCards.getCard(i);
                    // System.out.println("Card" + i + ": " + card2.toString());
                    who = thrownCards.getWhoCalled(card2);
                    //    System.out.println("Who" + i + ": " + who);
                    if (who == 1) {
                        //   System.out.println("ide már nem lép beeeeeeeeeeeee");
                        xx = whichImageViewContainsTheCard(card2, player1, cardsPlayer1);
                        if (xx != 9) {
                            fadeOutP1(cardsPlayer1.get(xx));
                            // player1.getCard(i).setThrown();
                        } else if (xx == 9) {
                            //     System.out.println("baj van, xx == 9");
                        }

                    }
                    if (who == 2) {
                        xx = whichImageViewContainsTheCard(card2, player2, cardsPlayer2);
                        if (xx != 9) {
                            fadeOut(cardsPlayer2.get(xx));
                            //    player2.getCard(i).setThrown();
                        }
                    }
                    if (who == 3) {
                        xx = whichImageViewContainsTheCard(card2, player3, cardsPlayer3);
                        if (xx != 9) {
                            fadeOut(cardsPlayer3.get(xx));
                            //   player3.getCard(i).setThrown();
                        }
                    }
                    if (who == 4) {
                        xx = whichImageViewContainsTheCard(card2, player4, cardsPlayer4);
                        if (xx != 9) {
                            fadeOut(cardsPlayer4.get(xx));
                            //   player4.getCard(i).setThrown();
                        }
                    }
                }

                //ő vitte a kört, a player numberét kaptam meg
                whoseRound = thrownCards.getPlayerNumberWhoIsFirst(deck.getTrumpSuit());

                //új kör jön, kiürítem a dobott lapok listáját
                thrownCards.resetThrownCards();
                //thrownCards.setWhoIsComing(1);
                // isDisabled = false;
                roundCounter--;
            }
        } else if (roundCounter == 0) {
            //  System.out.println("roundCounter = 0");
            calculateScore();

            // Animations.isFinished = false;
            // while(Animations.getIsFinished()) {
            // Animations.slideScore(scorePane, delay);
            //   resetGame(2);    
            //}
        }
    }

    public void launchNewRound() {
        deck.setOne(deck.getOne());
        if (deck.getOne() == 4) {
            player4.setRank(1);
            player1.setRank(2);
            player2.setRank(3);
            player3.setRank(4);
            //  cardToss(player4, cardsPlayer4);
        } else if (deck.getOne() == 3) {
            player3.setRank(1);
            player4.setRank(2);
            player1.setRank(3);
            player2.setRank(4);
            //  cardToss(player3, cardsPlayer3);
            //  cardToss(player4, cardsPlayer4);
        } else if (deck.getOne() == 2) {
            player2.setRank(1);
            player3.setRank(2);
            player4.setRank(3);
            player1.setRank(4);
            //  cardToss(player2, cardsPlayer2);
            //  cardToss(player3, cardsPlayer3);
            //  cardToss(player4, cardsPlayer4);
        } else if (deck.getOne() == 1) {
            player1.setRank(1);
            player2.setRank(2);
            player3.setRank(3);
            player4.setRank(4);
            //    System.out.println("P1 az első a körben, talán DO NOTHING");
        }
        calculateScore();
        Animations.slideScore(scorePane);

        resetGame(2);
        newRound = true;

    }
//Animáció

    public void fadeOut(ImageView card) {
        FadeTransition fade = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(1)
                .node(card)
                .delay(new Duration(4000))
                .toValue(0)
                .onFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent t) {
                        isDisabled = false;
                        btnBack.setDisable(false);
                        btnScore.setDisable(false);

                    }
                })
                .autoReverse(false)
                .build();
        fade.play();
    }
//Animáció

    public void fadeOutP1(ImageView card) {
        FadeTransition fade = FadeTransitionBuilder
                .create()
                .duration(new Duration(1200))
                .fromValue(1)
                .node(card)
                .delay(new Duration(4000))
                .toValue(0)
                .onFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent t) {
                        isDisabled = false;
                        btnBack.setDisable(false);
                        btnScore.setDisable(false);
                        if (roundCounter != 0) {
                            if (whoseRound == 4) {
                                player4.setRank(1);
                                player1.setRank(2);
                                player2.setRank(3);
                                player3.setRank(4);
                                //   enableThrownableCards();
                                cardToss(player4, cardsPlayer4);
                            } else if (whoseRound == 3) {
                                player3.setRank(1);
                                player4.setRank(2);
                                player1.setRank(3);
                                player2.setRank(4);
                                //   enableThrownableCards();
                                cardToss(player3, cardsPlayer3);
                                cardToss(player4, cardsPlayer4);
                            } else if (whoseRound == 2) {
                                player2.setRank(1);
                                player3.setRank(2);
                                player4.setRank(3);
                                player1.setRank(4);
                                //   enableThrownableCards();
                                cardToss(player2, cardsPlayer2);
                                cardToss(player3, cardsPlayer3);
                                cardToss(player4, cardsPlayer4);
                            } else if (whoseRound == 1) {
                                player1.setRank(1);
                                player2.setRank(2);
                                player3.setRank(3);
                                player4.setRank(4);
                                //  enableAllNotThrownCards();
                                //    System.out.println("P1 az első a körben, talán DO NOTHING");
                            } else {
                                //    System.out.println("Baj van, nincs több játékos - whoseRound");
                            }
                        }

                        if (player1.getRank() == 1) {
                            enableAllNotThrownCards();
                        }

                        if (roundCounter == 0) {
                            //isFinished = true;
                            // Animations anim = new Animations();
                            //  anim.launchThis();

                            //nem létező játékos, kinulláztam
                            //  deck.setTrumpChooser(0);
                            launchNewRound();

                        }
                    }
                })
                .autoReverse(false)
                .build();
        fade.play();
    }
//Animáció

    public void fadeOutTextLbl(Label label) {
        FadeTransition fade = FadeTransitionBuilder
                .create()
                .duration(new Duration(500))
                .fromValue(1)
                .node(label)
                .delay(new Duration(4000))
                .onFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent t) {
                        //  btnBack.setDisable(false);
                        animateExtraPoints();
                        if (player2.getRank() == 1) {
                            cardToss(player2, cardsPlayer2);
                            cardToss(player3, cardsPlayer3);
                            cardToss(player4, cardsPlayer4);

                        } else if (player3.getRank() == 1) {
                            cardToss(player3, cardsPlayer3);
                            cardToss(player4, cardsPlayer4);
                        } else if (player4.getRank() == 1) {
                            cardToss(player4, cardsPlayer4);
                        }
                    }

                })
                .toValue(0)
                .autoReverse(false)
                .build();
        fade.play();
    }
//Animáció

    public void fadeOutText(Label label) {
        FadeTransition fade = FadeTransitionBuilder
                .create()
                .duration(new Duration(500))
                .fromValue(1)
                .node(label)
                .delay(new Duration(4000))
                .onFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent t) {
                        btnBack.setDisable(false);
                        animateExtraPoints();

                    }
                })
                .toValue(0)
                .autoReverse(false)
                .build();
        fade.play();
    }
//Animáció

    public void fadeOutText3(Label label) {
        label.setOpacity(0);
        label.setVisible(true);
        FadeTransition fadeIn = FadeTransitionBuilder
                .create()
                .duration(new Duration(200))
                .fromValue(0)
                .node(label)
                //.delay(new Duration(300))

                .toValue(1)
                .autoReverse(false)
                .build();
        fadeIn.play();

    }
//Animáció

    public void fadeOutText2(Label label) {
        FadeTransition fadeIn = FadeTransitionBuilder
                .create()
                .duration(new Duration(100))
                .fromValue(0)
                .node(label)
                .delay(new Duration(0))
                .onFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent t) {
                        //    System.out.println("Fadeouttext2");
                        btnOne.setDisable(true);

                        int Rank = player1.getRank();
                        if (Rank == 1) {
                            trumpDecision(player2);
                            trumpDecision(player3);
                            trumpDecision(player4);
                            trumpCheck();
                        } else if (Rank == 2) {
                            trumpDecision(player2);
                            trumpDecision(player3);
                            trumpCheck();
                        } else if (Rank == 3) {
                            trumpDecision(player2);
                            trumpCheck();
                        } else if (Rank == 4) {
                            trumpCheck();
                        }

                    }

                })
                .toValue(1)
                .autoReverse(false)
                .build();
        fadeIn.play();

    }

    private void trumpCheck() {
        roundOne = 2;
        int Rank = player1.getRank();
        if (Rank == 1) {
            //visszaértek a P1-hez
            //ha 1, akkor mindenki továbbot mondott, minden btn aktív kivéve a tovább, s ő dönt
            if (wantTrump == 1) {
                btnOne.setDisable(false);
                btnTwo.setDisable(false);
                btnThree.setDisable(false);
                btnFour.setDisable(false);
                btnNext.setDisable(true);
            } else if (wantTrump == 2) {
                btnOne.setDisable(true);
                btnTwo.setDisable(true);
                btnThree.setDisable(false);
                btnFour.setDisable(false);
                btnNext.setDisable(false);
            } else if (wantTrump == 3) {
                btnOne.setDisable(true);
                btnTwo.setDisable(true);
                btnThree.setDisable(true);
                btnFour.setDisable(false);
                btnNext.setDisable(false);
            } else if (wantTrump == 4) {
                btnOne.setDisable(true);
                btnTwo.setDisable(true);
                btnThree.setDisable(true);
                btnFour.setDisable(true);
                btnNext.setDisable(true);
                endOfTrumpSelection(4, player4.getPlayerNumber());

            }
        } else if (Rank == 2) {
            btnNext.setDisable(false);
            btnOne.setDisable(true);
            btnTwo.setDisable(false);
            btnThree.setDisable(false);
            btnFour.setDisable(false);
        } else if (Rank == 3) {

            btnNext.setDisable(false);
            if (wantTrump == 1) {
                btnOne.setDisable(true);
                btnTwo.setDisable(false);
                btnThree.setDisable(false);
                btnFour.setDisable(false);
            } else if (wantTrump == 2) {
                btnOne.setDisable(true);
                btnTwo.setDisable(true);
                btnThree.setDisable(false);
                btnFour.setDisable(false);
            }
        } else if (Rank == 4) {
            btnNext.setDisable(false);
            if (wantTrump == 1) {
                btnOne.setDisable(true);
                btnTwo.setDisable(false);
                btnThree.setDisable(false);
                btnFour.setDisable(false);
            } else if (wantTrump == 2) {
                btnOne.setDisable(true);
                btnTwo.setDisable(true);
                btnThree.setDisable(false);
                btnFour.setDisable(false);
            } else if (wantTrump == 3) {
                btnOne.setDisable(true);
                btnTwo.setDisable(true);
                btnThree.setDisable(true);
                btnFour.setDisable(false);
            }

        }
    }

    //Tromfdöntés figyelése
    private void trumpDecision(Player player) {
        Random rand = new Random();
        //random választ a szín közül egyet, amit szeretne
        int x = rand.nextInt(4) + 1;
        if (!isEasyDiffOn) {
            //nehéz szint
            int cardSuitCheck = checkAreThree(player2);

            if (cardSuitCheck != 0 && (wantTrump < cardSuitCheck)) {
            }
        }

        //   System.out.println("WT TrumpDec:" + wantTrump + ", P: " + player.getPlayerNumber() + ", x= " + x);
        //ha az a szám kisebb az előző mondástól, akkor továbblép
        if (player.getRank() == 1) {
            wantTrump = 1;
            if (player.getPlayerNumber() == 2) {
                if (!isEasyDiffOn) {
                    //nehéz szint
                    int cardSuitCheck = checkAreThree(player);

                    if (cardSuitCheck != 0 && (wantTrump < cardSuitCheck)) {
                        player.setWantedTrump(cardSuitCheck);
                    } else {
                        player.setWantedTrump(x);
                    }
                }
                player.setWantedTrump(x);
                whoSaid = player.getPlayerNumber();
                p2.setText("Mondás: " + wantTrump);
                fadeOutText3(p2);
            } else if (player.getPlayerNumber() == 3) {
                if (!isEasyDiffOn) {
                    //nehéz szint
                    int cardSuitCheck = checkAreThree(player);

                    if (cardSuitCheck != 0 && (wantTrump < cardSuitCheck)) {
                        player.setWantedTrump(cardSuitCheck);
                    } else {
                        player.setWantedTrump(x);
                    }
                }
                player.setWantedTrump(x);
                whoSaid = player.getPlayerNumber();
                p3.setText("Mondás: " + wantTrump);
                fadeOutText3(p3);
            } else if (player.getPlayerNumber() == 4) {
                if (!isEasyDiffOn) {
                    //nehéz szint
                    int cardSuitCheck = checkAreThree(player);

                    if (cardSuitCheck != 0 && (wantTrump < cardSuitCheck)) {
                        player.setWantedTrump(cardSuitCheck);
                    } else {
                        player.setWantedTrump(x);
                    }
                }
                player.setWantedTrump(x);
                whoSaid = player.getPlayerNumber();
                p4.setText("Mondás: " + wantTrump);
                fadeOutText3(p4);
            }

        } else if (x <= wantTrump) {
            if (player.getPlayerNumber() == 2) {
                p2.setText("Tovább");
                fadeOutText3(p2);
            } else if (player.getPlayerNumber() == 3) {
                p3.setText("Tovább");
                fadeOutText3(p3);
            } else if (player.getPlayerNumber() == 4) {
                p4.setText("Tovább");
                fadeOutText3(p4);
            }
        } else if (x > wantTrump) {
            //ha nagyobb a szám, akkor bemondja
            if (player.getPlayerNumber() == 2) {
                wantTrump++;
                player.setWantedTrump(x);
                whoSaid = player.getPlayerNumber();
                p2.setText("Mondás: " + wantTrump);
                fadeOutText3(p2);
            } else if (player.getPlayerNumber() == 3) {
                wantTrump++;
                player.setWantedTrump(x);
                whoSaid = player.getPlayerNumber();
                p3.setText("Mondás: " + wantTrump);
                fadeOutText3(p3);
            } else if (player.getPlayerNumber() == 4) {
                wantTrump++;
                player.setWantedTrump(x);
                whoSaid = player.getPlayerNumber();
                p4.setText("Mondás: " + wantTrump);
                fadeOutText3(p4);
            }
        }
    }

    //Extra pontok animálása
    private void animateExtraPoints() {
        lblTrump.setText("");
        p2.setText("");
        p3.setText("");
        p4.setText("");
        checkExtraPoints();
        FadeTransition fadeIn1 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(0)
                .node(lblTrump)
                .toValue(1)
                .onFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent t) {
                        btnBack.setDisable(false);
                    }
                })
                .cycleCount(1)
                .autoReverse(false)
                .build();

        FadeTransition fadeOut1 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(1)
                .node(lblTrump)
                .toValue(0)
                .cycleCount(1)
                .autoReverse(false)
                .build();

        FadeTransition fadeIn2 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(0)
                .node(p2)
                .toValue(1)
                .cycleCount(1)
                .autoReverse(false)
                .build();

        FadeTransition fadeOut2 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(1)
                .node(p2)
                .toValue(0)
                .cycleCount(1)
                .autoReverse(false)
                .build();
        FadeTransition fadeIn3 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(0)
                .node(p3)
                .toValue(1)
                .cycleCount(1)
                .autoReverse(false)
                .build();

        FadeTransition fadeOut3 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(1)
                .node(p3)
                .toValue(0)
                .cycleCount(1)
                .autoReverse(false)
                .build();
        FadeTransition fadeIn4 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(0)
                .node(p4)
                .toValue(1)
                .cycleCount(1)
                .autoReverse(false)
                .build();

        FadeTransition fadeOut4 = FadeTransitionBuilder
                .create()
                .duration(new Duration(1000))
                .fromValue(1)
                .node(p4)
                .toValue(0)
                .cycleCount(1)
                .autoReverse(false)
                .build();
        PauseTransition pause = new PauseTransition(Duration.millis(5000));
        SequentialTransition seqT1 = new SequentialTransition(fadeIn1, pause, fadeOut1);
        SequentialTransition seqT2 = new SequentialTransition(fadeIn2, pause, fadeOut2);
        SequentialTransition seqT3 = new SequentialTransition(fadeIn3, pause, fadeOut3);
        SequentialTransition seqT4 = new SequentialTransition(fadeIn4, pause, fadeOut4);

        ParallelTransition parT = new ParallelTransition(seqT1, seqT2, seqT3, seqT4);
        parT.play();

    }

    //Extra pontok ellenőrzése
    private void checkExtraPoints() {

        //************** P1 ***************************************
        int suit = checkAreExactlyFour(player1);
        int kvartSuit = 0;
        int sub = 0;
        boolean kvart = false;
        boolean terc = false;
        List<Card> cardList = new ArrayList<>();
        if (suit != 0) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player1.getCard(i).getSuit() == suit) {
                    cardList.add(player1.getCard(i));
                }
            }

            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 4; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            //   System.out.println("Rendezett egyszínű lapok kvarthoz P1: " + out);
            sub = cardList.get(3).getValue() - cardList.get(2).getValue();
            if (sub == 1) {
                sub = cardList.get(2).getValue() - cardList.get(1).getValue();
                if (sub == 1) {
                    sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                    if (sub == 1) {
                        //   System.out.println("Kvart P1");
                        kvartSuit = suit;
                        kvart = true;

                    }
                }
            }
        }

        cardList.clear();
        suit = 0;
        sub = 0;
        suit = checkAreExactlyThree(player1);
        if (suit != 0 && suit != kvartSuit) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player1.getCard(i).getSuit() == suit) {
                    cardList.add(player1.getCard(i));
                }
            }
            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 3; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            //  System.out.println("Rendezett egyszínű lapok terchez P1: " + out);
            sub = cardList.get(2).getValue() - cardList.get(1).getValue();
            if (sub == 1) {
                sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                if (sub == 1) {
                    //  System.out.println("Terc P1");
                    terc = true;
                }
            }
        }

        if (kvart && terc) {
            lblTrump.setText("Kvart, terc!");
            //kell egy setter minden játékoshoz, majd a pontozásnál kiolvasom, hogy kinek mi volt és összesítem player1.setExtraScore(kvart, terc); - a pontozás után false-olom
            //    System.out.println("itt 1");
        } else if (kvart && !terc) {
            lblTrump.setText("Kvart!");
            //    System.out.println("itt 2");
        } else if (!kvart && terc) {
            lblTrump.setText("Terc");
            //    System.out.println("itt 3");
        }

        player1.setExtraScore(kvart, terc);

        //********************* P2 ************************
        kvartSuit = 0;
        suit = 0;
        sub = 0;
        kvart = false;
        terc = false;
        cardList.clear();
        suit = checkAreExactlyFour(player2);
        if (suit != 0) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player2.getCard(i).getSuit() == suit) {
                    cardList.add(player2.getCard(i));
                }
            }

            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 4; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            // System.out.println("Rendezett egyszínű lapok kvarthoz P2: " + out);
            sub = cardList.get(3).getValue() - cardList.get(2).getValue();
            if (sub == 1) {
                sub = cardList.get(2).getValue() - cardList.get(1).getValue();
                if (sub == 1) {
                    sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                    if (sub == 1) {
                        //     System.out.println("Kvart P2");
                        kvartSuit = suit;
                        kvart = true;

                    }
                }
            }
        }

        cardList.clear();
        suit = 0;
        sub = 0;
        suit = checkAreExactlyThree(player2);
        if (suit != 0 && suit != kvartSuit) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player2.getCard(i).getSuit() == suit) {
                    cardList.add(player2.getCard(i));
                }
            }
            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 3; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            //  System.out.println("Rendezett egyszínű lapok terchez P2: " + out);
            sub = cardList.get(2).getValue() - cardList.get(1).getValue();
            if (sub == 1) {
                sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                if (sub == 1) {
                    // System.out.println("Terc P2");
                    terc = true;
                }
            }
        }

        if (kvart && terc) {
            p2.setText("Kvart, terc!");
        } else if (kvart && !terc) {
            p2.setText("Kvart!");
        } else if (!kvart && terc) {
            p2.setText("Terc");
        }

        player2.setExtraScore(kvart, terc);
        //************** P3 ****************************************

        kvartSuit = 0;
        suit = 0;
        sub = 0;
        kvart = false;
        terc = false;
        cardList.clear();
        suit = checkAreExactlyFour(player3);
        if (suit != 0) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player3.getCard(i).getSuit() == suit) {
                    cardList.add(player3.getCard(i));
                }
            }

            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 4; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            //  System.out.println("Rendezett egyszínű lapok kvarthoz P3: " + out);
            sub = cardList.get(3).getValue() - cardList.get(2).getValue();
            if (sub == 1) {
                sub = cardList.get(2).getValue() - cardList.get(1).getValue();
                if (sub == 1) {
                    sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                    if (sub == 1) {
                        //  System.out.println("Kvart P3");
                        kvartSuit = suit;
                        kvart = true;

                    }
                }
            }
        }

        cardList.clear();
        suit = 0;
        sub = 0;
        suit = checkAreExactlyThree(player3);
        if (suit != 0 && suit != kvartSuit) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player3.getCard(i).getSuit() == suit) {
                    cardList.add(player3.getCard(i));
                }
            }
            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 3; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            //   System.out.println("Rendezett egyszínű lapok terchez P3: " + out);
            sub = cardList.get(2).getValue() - cardList.get(1).getValue();
            if (sub == 1) {
                sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                if (sub == 1) {
                    //  System.out.println("Terc P3");
                    terc = true;
                }
            }
        }

        if (kvart && terc) {
            p3.setText("Kvart, terc!");
        } else if (kvart && !terc) {
            p3.setText("Kvart!");
        } else if (!kvart && terc) {
            p3.setText("Terc");
        }

        player3.setExtraScore(kvart, terc);

        //************** P4 ****************************************
        kvartSuit = 0;
        suit = 0;
        sub = 0;
        kvart = false;
        terc = false;
        cardList.clear();
        suit = checkAreExactlyFour(player4);
        if (suit != 0) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player4.getCard(i).getSuit() == suit) {
                    cardList.add(player4.getCard(i));
                }
            }

            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 4; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            //   System.out.println("Rendezett egyszínű lapok kvarthoz P4: " + out);
            sub = cardList.get(3).getValue() - cardList.get(2).getValue();
            if (sub == 1) {
                sub = cardList.get(2).getValue() - cardList.get(1).getValue();
                if (sub == 1) {
                    sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                    if (sub == 1) {
                        //   System.out.println("Kvart P4");
                        kvartSuit = suit;
                        kvart = true;

                    }
                }
            }
        }

        cardList.clear();
        suit = 0;
        sub = 0;
        suit = checkAreExactlyThree(player4);
        if (suit != 0 && suit != kvartSuit) {
            for (int i = 0; i < 8; i++) {
                //feltöltöm a cardListet egyszínű kártyákkal
                if (player4.getCard(i).getSuit() == suit) {
                    cardList.add(player4.getCard(i));
                }
            }
            Collections.sort(cardList, new CardComparatorBySuit());
            String out = "";
            String eol = "\n";
            for (int i = 0; i < 3; i++) {
                out = out + cardList.get(i).toString() + eol;
            }
            //    System.out.println("Rendezett egyszínű lapok terchez P4: " + out);
            sub = cardList.get(2).getValue() - cardList.get(1).getValue();
            if (sub == 1) {
                sub = cardList.get(1).getValue() - cardList.get(0).getValue();
                if (sub == 1) {
                    //  System.out.println("Terc P4");
                    terc = true;
                }
            }
        }

        if (kvart && terc) {
            p4.setText("Kvart, terc!");
        } else if (kvart && !terc) {
            p4.setText("Kvart!");
        } else if (!kvart && terc) {
            p4.setText("Terc");
        }

        player4.setExtraScore(kvart, terc);

    }

    private void trumpSelection() {
        //  isNewTrump = true;
        btnBack.setDisable(true);
        deck.setTrumpChooser(0);
        scoreP1 = 0;
        scoreP2 = 0;
        roundCounter = 8;
        wantTrump = 0;
        // if (isNewTrump) {
        //    System.out.println("itt newgmae");
        //    stage = Stages.ALL_FALSE;
        //enableCards(stage);
        btnOne.setVisible(true);
        btnOne.setDisable(false);
        btnTwo.setVisible(true);
        btnTwo.setDisable(false);
        btnThree.setVisible(true);
        btnThree.setDisable(false);
        btnFour.setVisible(true);
        btnFour.setDisable(false);
        btnNext.setVisible(true);
        btnNext.setDisable(false);
        btnOne2.setVisible(false);
        btnOne2.setDisable(true);
        btnTwo2.setVisible(false);
        // btnTwo2.setDisable(false);
        btnThree2.setVisible(false);
        //  btnThree2.setDisable(false);
        btnFour2.setVisible(false);
        //btnFour2.setDisable(false);
        btnNext2.setVisible(false);
        //btnNext2.setDisable(false);
        roundOne = 1;
        if (player1.getRank() == 1) {

            // System.out.println("P1 a kényszeres");
            btnTwo.setDisable(true);
            btnThree.setDisable(true);
            btnFour.setDisable(true);
            btnNext.setDisable(true);

        } else if (player2.getRank() == 1) {
            //  System.out.println("P2 a kényszeres");
            trumpDecision(player2);
            trumpDecision(player3);
            trumpDecision(player4);
            trumpCheck();
        } else if (player3.getRank() == 1) {
            // System.out.println("P3 a kényszeres");
            trumpDecision(player3);
            trumpDecision(player4);
            trumpCheck();

        } else if (player4.getRank() == 1) {
            //  System.out.println("P4 a kényszeres");
            trumpDecision(player4);
            trumpCheck();
        }
    }

    //Pontok kiszámolása a kör végén
    private void calculatePointsOnRoundEnd() {
        int caller = 0;
        int score = 0;

        int max = thrownCards.getCard(0).getCardPoint(thrownCards.getCard(0).getValue(), false);
        int maxInd = 0;

        for (int i = 0; i < 4; i++) {
            if (thrownCards.getCard(i).getCardPoint(thrownCards.getCard(i).getValue(), false) > max) {
                max = thrownCards.getCard(i).getCardPoint(thrownCards.getCard(i).getValue(), false);
                maxInd = i;

            }
        }
        //   System.out.println("RoundCounter = " + roundCounter);
        caller = thrownCards.getWhoCalled(thrownCards.getCard(maxInd));
        // System.out.println("Max card: " + thrownCards.getCard(maxInd).toString());
        // System.out.println("WhoCalled: " + thrownCards.getWhoCalled(thrownCards.getCard(maxInd)));
        //  System.out.println("TrumpSuit: " + deck.getTrumpSuit());
        //  System.out.println("Caller :" + caller);

        if (caller == 1 || caller == 3) {
            for (int j = 0; j < 4; j++) {
                //  System.out.println("Bent P1");
                if (thrownCards.getCard(j).getSuit() == deck.getTrumpSuit()) {
                    score = score + thrownCards.getCard(j).getCardPoint(thrownCards.getCard(j).getValue(), true);
                } else {
                    score = score + thrownCards.getCard(j).getCardPoint(thrownCards.getCard(j).getValue(), false);
                }
            }

            if (roundCounter == 1) {
                //  System.out.println("Plus 10 pont P1-nek");
                score += 10;

            }
            scoreP1 += score;
        } else if (caller == 2 || caller == 4) {
            //  System.out.println("Bent P2");
            for (int j = 0; j < 4; j++) {
                if (thrownCards.getCard(j).getSuit() == deck.getTrumpSuit()) {
                    score = score + thrownCards.getCard(j).getCardPoint(thrownCards.getCard(j).getValue(), true);
                } else {
                    score = score + thrownCards.getCard(j).getCardPoint(thrownCards.getCard(j).getValue(), false);
                }
            }

            if (roundCounter == 1) {
                //  System.out.println("Plus 10 pont P2-nek");
                score += 10;

            }
            scoreP2 += score;
        }
        if (roundCounter == 1) {
            //terc, kvart összesítő a kör végén
            if (player1.getExtraScoreKvart()) {
                scoreP1 += 50;
                // System.out.println("kvart p1");
            }
            if (player1.getExtraScoreTerc()) {
                scoreP1 += 20;
                //  System.out.println("terc p1");
            }
            if (player3.getExtraScoreKvart()) {
                scoreP1 += 50;
                //  System.out.println("kvart p3");
            }
            if (player3.getExtraScoreTerc()) {
                scoreP1 += 20;
                //  System.out.println("terc p3");
            }

            //terc, kvart összesítő
            if (player2.getExtraScoreKvart()) {
                scoreP2 += 50;
                //  System.out.println("kvart p2");
            }
            if (player2.getExtraScoreTerc()) {
                scoreP2 += 20;
                //  System.out.println("terc p2");
            }
            if (player4.getExtraScoreKvart()) {
                scoreP2 += 50;
                //   System.out.println("kvart p4");
            }
            if (player4.getExtraScoreTerc()) {
                scoreP2 += 20;
                //  System.out.println("terc p4");
            }
        }

        // System.out.println("p1 score: " + scoreP1);
        // System.out.println("p2 score: " + scoreP2);
    }

    private void enableAllNotThrownCards() {
        int id;
        for (int i = 0; i < 8; i++) {
            if (player1.getCard(i).getThrown()) {
                id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                cardsPlayer1.get(id).setDisable(true);
            } else {
                id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                cardsPlayer1.get(id).setDisable(false);
            }
        }
    }

    private void enableThrownableCards() {
        int id;
        int tempColor = 0;
        int tempTrump = 0;
        for (int i = 0; i < 8; i++) {
            if (player1.getCard(i).getThrown()) {
                id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                cardsPlayer1.get(id).setDisable(true);
            } else {
                //ha dobható lap és a hívott szín lapja, akkor dobhatóra váltom
                if (player1.getCard(i).getSuit() == thrownCards.getCallColor()) {
                    id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                    cardsPlayer1.get(id).setDisable(false);
                    // System.out.println("Van szín");
                    tempColor++;
                } else {
                    id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                    cardsPlayer1.get(id).setDisable(true);
                }
            }
        }

        if (tempColor == 0) {
            // System.out.println("Nincs szín");
            for (int i = 0; i < 8; i++) {
                if (player1.getCard(i).getThrown()) {
                    id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                    cardsPlayer1.get(id).setDisable(true);
                } else {
                    //ha dobható lap és a hívot szín lapja, akkor dobhatóra váltom

                    if (player1.getCard(i).getSuit() == deck.getTrumpSuit()) {
                        id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                        //  System.out.println("Van tromf!");
                        cardsPlayer1.get(id).setDisable(false);
                        tempTrump++;
                    } else {
                        id = whichImageViewContainsTheCard(player1.getCard(i), player1, cardsPlayer1);
                        cardsPlayer1.get(id).setDisable(true);
                    }
                }
            }
        }

        if (tempTrump == 0 && tempColor == 0) {
            // System.out.println("Is-is");
            enableAllNotThrownCards();
        }
    }

    //Ledobás vizsgálat
    private int throwDownCheck(Player player) {
        List<Integer> cardsInHand = new ArrayList();
        for (int i = 0; i < 8; i++) {
            cardsInHand.add(player.getCard(i).getValue());
        }

        int freq1 = Collections.frequency(cardsInHand, 1);
        int freq2 = Collections.frequency(cardsInHand, 2);
        int freq3 = Collections.frequency(cardsInHand, 3);
        int freq4 = Collections.frequency(cardsInHand, 4);
        int freq5 = Collections.frequency(cardsInHand, 5);
        int freq6 = Collections.frequency(cardsInHand, 6);

        if (freq1 == 4) {
            return 1;
        }
        if (freq2 == 4) {
            return 2;
        }
        if (freq3 == 4) {
            return 3;
        }
        if (freq4 == 4) {
            return 4;
        }
        if (freq5 == 4) {
            return 5;
        }
        if (freq6 == 4) {
            return 6;
        }
        return 0;
    }

    //Animáció
    public void fadeIn(AnchorPane cat) {
        FadeTransition fadeI = FadeTransitionBuilder
                .create()
                .duration(new Duration(3000))
                .fromValue(0)
                .node(cat)
                .delay(new Duration(200))
                .toValue(1)
                .autoReverse(false)
                .build();

        FadeTransition fadeO = FadeTransitionBuilder
                .create()
                .duration(new Duration(2000))
                .fromValue(1)
                .node(cat)
                .delay(new Duration(500))
                .toValue(0)
                .onFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent t) {
                        isEnd = true;
                        Animations.slideScore(scorePane);
                        Animations.slideMenu(menuPane);
                        resetGame(3);
                    }
                })
                .autoReverse(false)
                .build();

        SequentialTransition seqT = new SequentialTransition(fadeI, fadeO);
        seqT.play();
    }

}
