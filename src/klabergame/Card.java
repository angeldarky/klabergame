/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package klabergame;

import javafx.scene.image.Image;

/**
 *
 * @author Komáromi Zsófia
 */
public class Card {
   

    public final static int MAKK = 1;
    public final static int ZOLD = 2;
    public final static int TOK = 3;
    public final static int PIROS = 4;
   
    
    private final int value;
    private final int suit;
    private final Image image;
  
    private boolean thrown;

    /**
     * Kártya konstruktora
     * @param theValue
     * @param theSuit
     * @param theImage
     */
    public Card(int theValue, int theSuit, Image theImage) {
        if (theSuit != MAKK && theSuit != PIROS && theSuit != TOK
                && theSuit != ZOLD) {
            throw new IllegalArgumentException("Illegal playing card suit");
        }
        if (theValue < 1 || theValue > 8) {
            throw new IllegalArgumentException("Illegal playing card value");
        }
        value = theValue;
        suit = theSuit;
        image = theImage;
        
    }

    /**
     * Szín visszaküldése
     * @return
     */
    public int getSuit() {
        return suit;
    }

    /**
     * Dobottra állítom
     */
    public void setThrown() {
        this.thrown = true;
    }
    
    /**
     * Megadja, hogy dobott-e
     * @return
     */
    public boolean getThrown() {
        return thrown;
    }
    
    

    /**
     *Dobhatóra állítja a kártyát
     */
        public void unsetThrown() {
        this.thrown = false;
    }

    /**
     * Pontok lekérése
     * @param Card
     * @param isTrump
     * @return
     */
    public int getCardPoint(int Card, boolean isTrump) {
        if (isTrump){
            switch(Card) {
                case 1:
                    return 11;
                case 2:
                    return 4;
                case 3:
                    return 20;
                case 4:
                    return 2;
                case 5:
                    return 10;
                case 6:
                    return 14;
                default:
                    return 0;
            }
        } else {
            switch(Card) {
                case 1:
                    return 11;
                case 2:
                    return 4;
                case 3:
                    return 3;
                case 4:
                    return 2;
                case 5:
                    return 10;
                default:
                    return 0;
            }
        }
    }

    /**
     * Érték lekérése
     * @return
     */
    public int getValue() {
        return value;
    }

    /**
     * Szín stringként
     * @return
     */
    public String getSuitAsString() {
        switch (suit) {
            case MAKK:
                return "MAKK";
            case ZOLD:
                return "ZOLD";
            case TOK:
                return "TOK";
            default:
                return "PIROS";
        }
    }

    /**
     * Érték stringként
     * @return
     */
    public String getValueAsString() {

        switch (value) {
            case 1:
                return "Ász";
            case 5:
                return "X";
            case 2:
                return "Csikó";
            case 3:
                return "Felső";
            case 4:
                return "Alsó";
            case 6:
                return "IX";
            case 7:
                return "VIII";
            case 8: 
                return "VII";
            
            default:
                return "Default";

        }
    }
    
    /**
     * Kép megadása
     * @return
     */
    public Image getImage() {
        return image;
    }

   
    @Override
    public String toString() {
        return getValueAsString() + " of " + getSuitAsString();
    }
}